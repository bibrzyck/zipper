package zipper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class ZipUncompress {
public static void main(String...strings){
	try(FileInputStream fis=new FileInputStream("erlambda.zip");
			ZipInputStream zis=new ZipInputStream(fis)){
		ZipEntry ze;
		while((ze=zis.getNextEntry())!=null){
			try(FileOutputStream fos=new FileOutputStream(ze.getName())){
				int buflen=1024,c;
				byte[] buffer=new byte[buflen];
				while((c=zis.read(buffer, 0, buflen))>-1){
					fos.write(buffer, 0, c);
				}
				System.out.println("Wypakowano plik "+ze.getName());
			}
		}
		System.out.println("Zakończono dekompresję.");
		
	} catch (FileNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
}
}
