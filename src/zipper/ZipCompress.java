package zipper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class ZipCompress {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try(FileInputStream fis=new FileInputStream("erlambda.pdf");
				FileOutputStream fos=new FileOutputStream("erlambda.zip");
				ZipOutputStream zos=new ZipOutputStream(fos)
				){
			int buflen=1024,c;
			byte[] buffer = new byte[buflen];
			ZipEntry ze=new ZipEntry("erlambda.pdf");
			zos.putNextEntry(ze);
			while((c=fis.read(buffer, 0, buflen))>-1){
				zos.write(buffer, 0, c);
			}
			zos.closeEntry();
			System.out.println("Kompresja zakończona");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
